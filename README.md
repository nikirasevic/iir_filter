<html>

<body>

<p class="padding-bottom:3cm;">
This repository contains the project files for Digital Parametric EQ design.
</p>

<p class="padding-bottom:3cm;">
Transfer function of the filter is:
</p>

<p class="padding-bottom:3cm;">
y(n) = −(f<sub>0</sub><sup>2</sup> + f<sub>0</sub>·q − 2)y(n − 1) − (1 − f<sub>0</sub>·q)y(n − 2) + f<sub>0</sub>u(n) − f<sub>0</sub>u(n − 1)
</p>

</body>
</html>
