----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Ra�evi�
-- 
-- Create Date: 05/30/2020 07:26:03 PM
-- Design Name: 
-- Module Name: iir_module_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_unsigned.all;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use work.txt_util.all;
use work.util_pkg.all;

entity iir_module_tb is
    generic(
        data_width  : natural := 24;
        iir_ord     : natural := 2
    );
end iir_module_tb;

architecture beh of iir_module_tb is
    signal clk_s    : std_logic := '0';
    signal rst_s    : std_logic := '0';
    signal we_s     : std_logic := '0';
    signal ready_s  : std_logic;
      
    signal data_i_s : std_logic_vector(data_width-1 downto 0);
    signal data_o_s : std_logic_vector(data_width-1 downto 0);
    
 --   signal q_s : std_logic_vector(data_width-1 downto 0);
 --   signal f_s : std_logic_vector(data_width-1 downto 0);
    
    constant period         : time := 20ns;
    constant reset_time     : time := 10us;   
    --putanju do �eljenog fajla je potrebno prilagoditi strukturi direktorijuma na ra�unaru na kome se vr�i provera rada.
    file input_test_vector      : text open read_mode is "C:\Users\kakaroto\Documents\Projects\Digital_Parametric-EQ\design\iir_filter\filter_inputs\wav\input.txt";
   -- file output_check_vector    : text open read_mode is "C:\Users\kakaroto\Documents\Projects\Digital_Parametric-EQ\design\iir_filter\filter_inputs\wav\output.txt";
   -- file input_coef_a           : text open read_mode is "C:\Users\kakaroto\Documents\Projects\Digital_Parametric-EQ\design\iir_filter\filter_inputs\wav\coef_hex_a.txt";
   -- file input_coef_b           : text open read_mode is "C:\Users\kakaroto\Documents\Projects\Digital_Parametric-EQ\design\iir_filter\filter_inputs\wav\coef_hex_b.txt";
    
   -- signal coef_addr_i_s    : std_logic_vector(2 downto 0);
   -- signal coef_a_i_s       : std_logic_vector(data_width-1 downto 0);
   -- signal coef_b_i_s       : std_logic_vector(data_width-1 downto 0);
    
    signal start_check : std_logic := '0';

begin
    
    
    iir_module_under_test:
    entity work.iir_module(beh)
    generic map(
        iir_ord     =>  iir_ord,
        data_width  =>  data_width
    )
    port map(
        clk_i   =>  clk_s,
        rst_i   =>  rst_s,
        we_i    =>  we_s,
        ready_o =>  ready_s,
  --      q_i     =>  q_s,
  --      f_i     =>  f_s,
        data_i  =>  data_i_s,
        data_o  =>  data_o_s
    );
    
    rst_s <= '1'after reset_time;
             
    clk_process:
    process
    begin
        clk_s <= '0';
        wait for period/2;
        clk_s <= '1';
        wait for period/2;
    end process;
    
    stim_process:
    process
        variable tv   : line;
    begin
--        --upis koeficijenata
        data_i_s <= (others=>'0');
       -- rst_s <= '0';
--        wait until falling_edge(clk_s);
--        for i in 0 to iir_ord loop
--            we_s <= '1';
--            coef_addr_i_s <= std_logic_vector(to_unsigned(i,log2c(iir_ord+1)));
--            readline(input_coef_a,tv);
--            coef_a_i_s <= to_std_logic_vector(string(tv));
--            wait until falling_edge(clk_s);
--        end loop;
--        wait until falling_edge(clk_s);
--        for j in 0 to iir_ord loop
--            we_s <= '1';
--            coef_addr_i_s <= std_logic_vector(to_unsigned(j,log2c(iir_ord+1)));
--            readline(input_coef_b,tv);
--            coef_b_i_s <= to_std_logic_vector(string(tv));
--            wait until falling_edge(clk_s);
--        end loop;
        --ulaz za filtriranje
       -- if ready_s = '1' then
            while not endfile(input_test_vector) loop
                readline(input_test_vector,tv);
                data_i_s <= to_std_logic_vector(string(tv));
                wait until falling_edge(clk_s);
            end loop;
            report "verification done!" severity failure;
       -- end if;
    end process;
    
end beh;
