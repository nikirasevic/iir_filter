----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 05/30/2020 05:41:23 PM
-- Design Name: 
-- Module Name: iir_module - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.util_pkg.all;

entity iir_module is
    generic(    
        iir_ord     : natural := 2;
        data_width  : natural := 24
    );
                
    Port ( 
        clk_i           : in    std_logic;
        rst_i           : in    std_logic;
        we_i            : in    std_logic;
        ready_o         : out   std_logic;    
     --   q_i             : in    std_logic_vector (data_width-1 downto 0);
     --   f_i             : in    std_logic_vector (data_width-1 downto 0);
        data_i          : in    std_logic_vector (data_width-1 downto 0);
        data_o          : out   std_logic_vector (data_width-1 downto 0)
    );
    
end iir_module;

architecture beh of iir_module is
      
    type std_2d is array (iir_ord-1 downto 0) of std_logic_vector(2*data_width-1 downto 0);
    signal mac_inter : std_2d := (others=>(others=>'0'));
    
    signal data_o_s     : std_logic_vector (data_width-1 downto 0);
    signal adder_s      : std_logic_vector (2*data_width-1 downto 0);
    signal multi_s      : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal sqr_f_s      : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal multi_fq_s   : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal adder_fq_1_s : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal adder_fq_2_s : std_logic_vector (2*data_width-1 downto 0) := (others=>'0');
    signal not_f_s      : std_logic_vector (data_width-1 downto 0);
   
   
    signal ready_s  : std_logic := '0';
    
    type coef_t is array (0 to iir_ord) of std_logic_vector(data_width-1 downto 0);
    signal a_s : coef_t := (others=>(others=>'0'));
    signal b_s : coef_t := (others=>(others=>'0'));
    
    signal q_s : std_logic_vector (data_width-1 downto 0) := x"01999a";
    signal f_s : std_logic_vector (data_width-1 downto 0) := x"048b38";
    
    --signal a_s : coef_t := (b"000100000000000000000000",
    --                        b"000111100100000101010110",
    --                        b"111100000111010001010010");
                           
    --signal b_s : coef_t := (b"000001001000101100111000",
    --                        b"111110110111010011001000",
    --                        b"000000000000000000000000");                          
                          
begin

    
    --  a = [1 (f0^2+f0*q-2) (1 - f0*q)]
    --  b = [f0 -f0 0]
    multi_fq_s <= std_logic_vector((signed(f_s) * signed(q_s)));
    sqr_f_s <= std_logic_vector((signed(f_s) * signed(f_s)));
    adder_fq_1_s <= std_logic_vector(2 - signed(multi_fq_s) - signed(sqr_f_s));
    adder_fq_2_s <= std_logic_vector(signed(multi_fq_s) - 1);
    not_f_s <= not f_s;
    
    
    process
    begin
   --     if rst_i = '0' then
            a_s(1) <= adder_fq_1_s(43 downto 20);
            a_s(2) <= adder_fq_2_s(43 downto 20);
            b_s(0) <= f_s;
            b_s(1) <= std_logic_vector(signed(not_f_s) + 1);
            wait until falling_edge(clk_i);
            ready_s <= '1';
   --     end if;
    end process;
    
    
    first_MAC:
    entity work.mac(beh)
    generic map(
        N_bit_in => data_width,
        N_bit_out => 2*data_width
    )
    port map(
        clk => clk_i,
        rst => rst_i,    
        u_i => data_i,
        y_i => data_o_s,
        a_i => a_s(2),
        b_i => b_s(2),
        mac_i => (others=>'0'),
        mac_o => mac_inter(0)
    );
    
    second_MAC:
    entity work.mac(beh)
    generic map(
        N_bit_in => data_width,
        N_bit_out => 2*data_width
    )
    port map(
        clk => clk_i,
        rst => rst_i,
        u_i => data_i,
        y_i => data_o_s,
        a_i => a_s(1),
        b_i => b_s(1),
        mac_i => mac_inter(0),
        mac_o => mac_inter(1)
    );
    
    
    multi_s <= std_logic_vector(signed(data_i) * signed(b_s(0)));

    adder_s <= std_logic_vector(signed(multi_s) + signed(mac_inter(1)));

    data_o_s <= adder_s(43 downto 20);


    process (clk_i)
    begin
        if(rising_edge(clk_i)) then 
            if ready_s = '1' then
                           
                data_o <= data_o_s;
                
            end if;
        end if;
    end process;
         
    ready_o <= ready_s;

end beh;
