----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 05/30/2020 06:47:32 PM
-- Design Name: 
-- Module Name: mac - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity mac is
    generic(    
        N_bit_in : natural;
        N_bit_out : natural
    );
                
    Port ( 
        clk     : in    std_logic;
        rst     : in    std_logic;   
        u_i     : in    std_logic_vector (N_bit_in-1 downto 0);
        y_i     : in    std_logic_vector (N_bit_in-1 downto 0);
        a_i     : in    std_logic_vector (N_bit_in-1 downto 0);
        b_i     : in    std_logic_vector (N_bit_in-1 downto 0);
        mac_i   : in    std_logic_vector (N_bit_out-1 downto 0);
        mac_o   : out   std_logic_vector (N_bit_out-1 downto 0)
    );
    
end mac;

architecture beh of mac is
    signal reg_s        : std_logic_vector (N_bit_out-1 downto 0) := (others=>'0');
    signal sum_s        : std_logic_vector (N_bit_out-1 downto 0) := (others=>'0');
    signal multi_a_s    : std_logic_vector (N_bit_out-1 downto 0) := (others=>'0');
    signal multi_b_s    : std_logic_vector (N_bit_out-1 downto 0) := (others=>'0'); 
begin

    multi_b_s <= std_logic_vector(signed(y_i) * signed(a_i));
    
    multi_a_s <= std_logic_vector(signed(u_i) * signed(b_i));
        
    sum_s <= std_logic_vector(signed(multi_b_s) + signed(multi_a_s));
  
    reg_s <= std_logic_vector(signed(sum_s) + signed(mac_i));
           
    shif_register:
    entity work.reg(beh)
    generic map(N_bit_reg => N_bit_out)
    port map(   
        clk => clk,
        rst => rst,
        d => reg_s,
        q => mac_o
    );      
    
   
end beh;
