----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 05/30/2020 09:59:16 PM
-- Design Name: 
-- Module Name: reg - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity reg is
    generic( N_bit_reg : natural);
                
    Port ( 
        clk : in    std_logic;
        rst : in    std_logic;
        d   : in    std_logic_vector (N_bit_reg - 1 downto 0);
        q   : out   std_logic_vector (N_bit_reg - 1 downto 0)
    );
    
end reg;

architecture beh of reg is

begin
    process (clk, rst) is
    begin
        if(rst = '0') then
            q <= (others=>'0');
        elsif(rising_edge(clk)) then
            q <= d;
         end if;
    end process;
    
end beh;
