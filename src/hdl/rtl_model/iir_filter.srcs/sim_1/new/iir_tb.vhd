----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Ra�evi�
-- 
-- Create Date: 05/30/2020 07:26:03 PM
-- Design Name: 
-- Module Name: iir_module_tb - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use std.textio.all;
use work.txt_util.all;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity iir_tb is
    generic(in_out_data_width : natural := 24;
            iir_ord : natural := 2);
--  Port ( );
end iir_tb;

architecture beh of iir_tb is
    signal clk_s : std_logic := '0';
    signal rst_s : std_logic := '0';
    signal uut_input_s : std_logic_vector(in_out_data_width-1 downto 0);
    signal uut_output_s : std_logic_vector(in_out_data_width-1 downto 0);

    constant per_c      : time := 20ns;
    constant T_RESET 	: time := 20ns; 		-- reset time
    --putanju do �eljenog fajla je potrebno prilagoditi strukturi direktorijuma na ra�unaru na kome se vr�i provera rada.
    file input_test_vector : text open read_mode is "C:\Users\kakaroto\Documents\Projects\Digital_Parametric-EQ\design\iir_filter\filter_inputs\input.txt";

begin
    
    rst_s <= '1'after T_RESET;
    
    iir_under_test:
    entity work.iir(rtl)
    port map(
        clk_i => clk_s,
        rst => rst_s,   
        u_i=>uut_input_s,
        y_o=>uut_output_s
    );
             
    clk_process:
    process
    begin
        clk_s <= '0';
        wait for per_c/2;
        clk_s <= '1';
        wait for per_c/2;
    end process;
    
    stim_process:
    process
        variable tv : line;
    begin
        uut_input_s <= (others=>'0');
        wait until falling_edge(clk_s);
        --ulaz za filtriranje
        while not endfile(input_test_vector) loop
            readline(input_test_vector,tv);
            uut_input_s <= to_std_logic_vector(string(tv));
            wait until falling_edge(clk_s);
        end loop;
        report "verification done!" severity failure;
    end process;

end beh;
