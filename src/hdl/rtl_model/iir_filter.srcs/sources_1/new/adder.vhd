----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/01/2020 09:45:02 PM
-- Design Name: 
-- Module Name: adder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity adder is
    generic(N_bit_adder : natural);
                
    Port ( 
        a_add_i     : in    STD_LOGIC_VECTOR (N_bit_adder - 1 downto 0);
        b_add_i     : in    STD_LOGIC_VECTOR (N_bit_adder - 1 downto 0);
        res_add_o   : out   STD_LOGIC_VECTOR (N_bit_adder - 1 downto 0)
    );
    
end adder;

architecture beh of adder is
    
begin

    res_add_o <= std_logic_vector(signed(a_add_i) + signed(b_add_i));

end beh;