----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 06/01/2020 09:45:02 PM
-- Design Name: 
-- Module Name: delayer1 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity delayer1 is
    generic(N_bit_del : natural);
    Port ( 
        clk     : in    std_logic;
        rst_d   : in    std_logic;
        d_del   : in    std_logic_vector (N_bit_del - 1 downto 0);
        q_del   : out   std_logic_vector (N_bit_del - 1 downto 0)
    );
    
end delayer1;

architecture beh of delayer1 is
    
    component paralleldff is 							
		generic (N_bit_reg : natural); 					
		port (
		  clk   : in std_logic;
		  resetn: in std_logic;
		  d 	: in std_logic_vector(N_bit_reg - 1 downto 0); 
		  q 	: out std_logic_vector(N_bit_reg - 1 downto 0)
		);
	end component paralleldff;
	
begin

    first_delay: paralleldff
    generic map(N_bit_reg => N_bit_del)
    port map(   
        clk => clk,
        resetn => rst_d,
        d => d_del,
        q => q_del
    );
                                
end beh;
