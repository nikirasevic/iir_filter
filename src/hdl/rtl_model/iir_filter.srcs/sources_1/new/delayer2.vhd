----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 06/01/2020 09:45:02 PM
-- Design Name: 
-- Module Name: delayer2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity delayer2 is
    generic( N_bit_del2 : natural);
    Port ( 
        clk     : in    std_logic;
        rst_d2  : in    std_logic;
        d_del2  : in    std_logic_vector (N_bit_del2 - 1 downto 0);
        q_del2  : out   std_logic_vector (N_bit_del2 - 1 downto 0)
    );

end delayer2;

architecture beh of delayer2 is

    component paralleldff is 							
		generic (N_bit_reg : natural); 					
		port (
		  clk   : in std_logic;
		  resetn: in std_logic;
		  d 	: in std_logic_vector(N_bit_reg - 1 downto 0); 
		  q 	: out std_logic_vector(N_bit_reg - 1 downto 0)
		);
	end component paralleldff;
	
    signal q_z1 : std_logic_vector(N_bit_del2 - 1 downto 0);

begin

    first_delay: paralleldff
    generic map(N_bit_reg => N_bit_del2)
    port map(   
        clk => clk,
        resetn => rst_d2,
        d => d_del2,
        q => q_z1
    );
    
    second_delay: paralleldff
    generic map(N_bit_reg => N_bit_del2)
    port map(   
        clk => clk,
        resetn => rst_d2,
        d => q_z1,
        q => q_del2
    );
                
end beh;
