----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 06/02/2020 10:03:49 AM
-- Design Name: 
-- Module Name: fulladder - beh
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fulladder is
    Port ( 
        a       : in STD_LOGIC;
        b       : in STD_LOGIC;
        cin     : in STD_LOGIC;
        cout    : out STD_LOGIC;
        s       : out STD_LOGIC
    );
    
end fulladder;

 architecture beh of fulladder is

begin

    s <= a xor b xor cin;
    
    cout <= (a and b) or (a and cin) or (b and cin);


end beh;
