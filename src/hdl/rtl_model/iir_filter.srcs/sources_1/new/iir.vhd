----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 06/01/2020 09:45:02 PM
-- Design Name: 
-- Module Name: iir - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity iir is
    generic ( data_width : natural := 24);
    Port ( 
        clk_i   : in    std_logic;
        rst     : in    std_logic;
        u_i     : in    std_logic_vector (data_width - 1 downto 0);
        y_o     : out   std_logic_vector (data_width - 1 downto 0)
    );
        
end iir;

architecture rtl of iir is
    
    component delayer1 is 						
		generic (N_bit_del : natural); 						
		port (
		  clk     : in std_logic;
		  rst_d   : in std_logic;
		  d_del   : in std_logic_vector(N_bit_del - 1 downto 0);  
		  q_del   : out std_logic_vector(N_bit_del - 1 downto 0)
		);
	end component delayer1;
	
	component delayer2 is 							
        generic (N_bit_del2 : natural); 			
		port (
		  clk     : in std_logic;
		  rst_d2  : in std_logic;
		  d_del2  : in std_logic_vector(N_bit_del2 - 1 downto 0);  
		  q_del2  : out std_logic_vector(N_bit_del2 - 1 downto 0)
		);
	end component delayer2;
  
    component RCA is
        generic (N_bit_rca : natural);
        port (
            aN      : in std_logic_vector(N_bit_rca - 1 downto 0);
            bN 		: in std_logic_vector(N_bit_rca - 1 downto 0);	
            cinN	: in std_logic;	 							
            coutN 	: out std_logic;	 						
            sN 		: out std_logic_vector(N_bit_rca - 1 downto 0)
        );
    end component RCA;
        
    component multiplier is 							
        generic (
            N_bit_multi_in : natural;
            N_bit_multi_out : natural
        ); 			
        port (
            a_multi_i   : in std_logic_vector(N_bit_multi_in - 1 downto 0); 
            b_multi_i   : in std_logic_vector(N_bit_multi_in - 1 downto 0);
            res_multi_o : out std_logic_vector(N_bit_multi_out - 1 downto 0)
        );
    end component multiplier;
    
    
    ------------------------------------------------------------------------
    -- Filter coeffs - got from Matlab - 24bit signed, fixed point in format 4.20
    
    type coef_t is array (0 to 2) of std_logic_vector(data_width - 1 downto 0);
    signal a : coef_t := (  x"100000",
                            x"1fce0d",
                            x"f01d2c");
                          
    signal b : coef_t := (  x"0123bb",
                            x"fedc45",
                            x"000000");
    
    ------------------------------------------------------------------------
    
    signal u_delay      : std_logic_vector (data_width - 1 downto 0) := (others=>'0');
    signal y_delay      : std_logic_vector (data_width - 1 downto 0) := (others=>'0');
    signal y_delay2     : std_logic_vector (data_width - 1 downto 0) := (others=>'0');
    
    signal y1_multi     : std_logic_vector (2*data_width - 1 downto 0);
    signal y2_multi     : std_logic_vector (2*data_width - 1 downto 0);
    signal u1_multi     : std_logic_vector (2*data_width - 1 downto 0);
    signal u2_multi     : std_logic_vector (2*data_width - 1 downto 0);
    
    signal y_add        : std_logic_vector (2*data_width - 1 downto 0); 
    signal u_add        : std_logic_vector (2*data_width - 1 downto 0);
    
    signal y_o_i        : std_logic_vector (2*data_width - 1 downto 0) := (others=>'0');
--    signal y_o_s        : std_logic_vector (data_width - 1 downto 0) := (others=>'0');
    
    signal co1 	: std_logic;
    signal co2 	: std_logic;
    signal co3 	: std_logic;

    
begin

    -- Input delay

    delay_u: delayer1
    generic map(N_bit_del => data_width)
    port map(  
        clk => clk_i,
        rst_d => rst,
        d_del => u_i,  
	    q_del => u_delay  
    );
    
    --Output delay
    
    delay_y: delayer1
    generic map(N_bit_del => data_width)
    port map(  
        clk => clk_i,
        rst_d => rst,
        d_del => y_o_i(43 downto 20),  
	    q_del => y_delay 
    );
    
    delay_y2: delayer2
    generic map(N_bit_del2 => data_width)
    port map(  
        clk => clk_i,
        rst_d2 => rst,
        d_del2 => y_o_i(43 downto 20),  
	    q_del2 => y_delay2 
    );
    
    -- Input multiplier
    
    multi_u1: multiplier
    generic map(
        N_bit_multi_in => data_width,
        N_bit_multi_out => 2*data_width
    )
    port map(
        a_multi_i => b(0),
        b_multi_i => u_i,
        res_multi_o => u1_multi
    );
    
    multi_u2: multiplier
    generic map(
        N_bit_multi_in => data_width,
        N_bit_multi_out => 2*data_width
    )
    port map(
        a_multi_i => b(1),
        b_multi_i => u_delay,
        res_multi_o => u2_multi
    );
    
    -- Output multiplier
    
    multi_y1: multiplier
    generic map(
        N_bit_multi_in => data_width,
        N_bit_multi_out => 2*data_width
    )
    port map(
        a_multi_i => a(1),
        b_multi_i => y_delay,
        res_multi_o => y1_multi
    );
    
    multi_y2: multiplier
    generic map(
        N_bit_multi_in => data_width,
        N_bit_multi_out => 2*data_width
    )
    port map(
        a_multi_i => a(2),
        b_multi_i => y_delay2,
        res_multi_o => y2_multi
    );
   
    -- Input adder
    
    add_u: RCA
        generic map (N_bit_rca => 2*data_width)
	    port map(
            aN => u1_multi, 		
            bN => u2_multi,
            cinN => '0',
            coutN => co1,
            sN => u_add
        );
    
    -- Output adder
    
    add_y: RCA
        generic map (N_bit_rca => 2*data_width)
	    port map(
            aN => y1_multi, 		
            bN => y2_multi,
            cinN => '0',
            coutN => co2,
            sN => y_add
        );
        
        
    -- Output signal
        
    add_y_o: RCA
    generic map (N_bit_rca => 2*data_width)
    port map(
        aN => u_add, 		
        bN => y_add,
        cinN => '0',
        coutN => co3,
        sN => y_o_i
    );
    
    y_o <= y_o_i(43 downto 20);

end rtl;

    
