----------------------------------------------------------------------------------
-- Company: FTN
-- Engineer: Nikola Rasevic
-- 
-- Create Date: 06/01/2020 09:45:02 PM
-- Design Name: 
-- Module Name: multiplier - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity multiplier is
    generic(
        N_bit_multi_in : natural;
        N_bit_multi_out : natural
    );
                
    Port ( 
        a_multi_i   : in    STD_LOGIC_VECTOR (N_bit_multi_in - 1 downto 0);
        b_multi_i   : in    STD_LOGIC_VECTOR (N_bit_multi_in - 1 downto 0);
        res_multi_o : out   STD_LOGIC_VECTOR (N_bit_multi_out - 1 downto 0)
    );
    
end multiplier;

architecture beh of multiplier is

begin

    res_multi_o <= std_logic_vector(signed(a_multi_i) * signed(b_multi_i));
    
end beh;
