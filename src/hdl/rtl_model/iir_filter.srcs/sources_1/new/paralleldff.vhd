----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/01/2020 09:45:02 PM
-- Design Name: 
-- Module Name: paralleldff - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity paralleldff is
    generic(N_bit_reg : natural);
                
    Port ( 
        clk     : in    STD_LOGIC;
        resetn  : in    STD_LOGIC;
        d       : in    STD_LOGIC_VECTOR (N_bit_reg - 1 downto 0);
        q       : out   STD_LOGIC_VECTOR (N_bit_reg - 1 downto 0)
    );

end paralleldff;

architecture beh of paralleldff is

begin
    process (clk) is
    begin
        if(resetn = '0') then 					-- Asynchronous reset model
			q <= (others => '0'); 				-- clear output

		elsif(rising_edge(clk)) then
            q <= d;
         end if;
    end process;
    
end beh;