clear all
clc

fs = 88200;     % Sample rate
n = 0 : 1999;   % Discrete time

fc = 1000;   % Filter central frequency in Hz
Q = 10;      % Filter Q factor (Q = fc/BW)

f0 = 2*sin((pi*fc)/fs)  % Central frequency coefficient for digital filter
q = 1/Q                 % Q factor coefficient for digital filter

%   Input signal definition
mag = [5 10 0 1 0];
f = [500 1000 2000 5000 8000];

x1 = mag(1)*cos(2 * pi * f(1)/fs * n);
x2 = mag(2)*cos(2 * pi * f(2)/fs * n);
x3 = mag(3)*cos(2 * pi * f(3)/fs * n);
x4 = mag(4)*cos(2 * pi * f(4)/fs * n);
x5 = mag(5)*cos(2 * pi * f(5)/fs * n);

u = x1 + x2 + x3 + x4 + x5;     % Input signal
u_sim = [n;u]';                 % Simulink workspace variable

%   Filter coeficients obtained from Input/Output equation
a = [1 (f0^2+f0*q-2) (1-f0*q)]
b = [f0 -f0 0]

%   IIR filter realization using function filter
y = filter(b,a,u);

%   Input and output signal plots
figure (1);
subplot(2,1,1), stem(n,u), title ('Input signal of 2000 samples'); xlabel('Discrete time'); ylabel('Magnitude');
subplot(2,1,2), stem(n,y), title ('Output signal of 2000 samples, calculated using function filter, fc = 1000 Hz'); xlabel('Discrete time'); ylabel('Magnitude');

%	Frequency response calculation
N_fft = 1024;
n1 = 0:N_fft/2-1;
w = n1*fs/(2*(N_fft/2-1));

[H_nf, f] = freqz(b, a, N_fft, 'whole', fs);
Ha_nf= abs(H_nf(1:N_fft/2));
Hp_nf = 180*unwrap(angle(H_nf(1:N_fft/2)))/pi;

%   Magnitude respose plot
figure(2);
plot (w, 20*log10(Ha_nf), 'LineWidth', 1); axis ([0 fs/8 -40 40]); title ('IIR filter magnitude response'); xlabel('Discrete frequency'); ylabel('Magnitude');

%   Phase respose plot
figure(3);
plot (w, Hp_nf, 'LineWidth', 1); axis ([0 fs/8 -100 100]); title ('IIR filter phase response'); xlabel('Discrete frequency'); ylabel('Angle');

