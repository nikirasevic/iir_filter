clear all
clc

fs = 88200;     % Sample rate
n = 0 : 1999;   % Discrete time

fc = 500:500:10000;     % Filter central frequency in Hz
Q = 5;                  % Filter Q factor (Q = fc/BW)

f0 = 2*sin((pi*fc)/fs)  % Central frequency coefficient for digital filter
q = 1/Q                 % Q factor coefficient for digital filter

%   Input signal definition
mag = [5 10 0 1 0];
f = [500 1000 2000 5000 8000];

x1 = mag(1)*cos(2 * pi * f(1)/fs * n);
x2 = mag(2)*cos(2 * pi * f(2)/fs * n);
x3 = mag(3)*cos(2 * pi * f(3)/fs * n);
x4 = mag(4)*cos(2 * pi * f(4)/fs * n);
x5 = mag(5)*cos(2 * pi * f(5)/fs * n);

u = x1 + x2 + x3 + x4 + x5;     % Input signal
u_sim = [n;u]';                 % Simulink workspace variable

%   Frequency response parameters 
N_fft = 1024;
n1 = 0:N_fft/2-1;
w = n1*fs/(2*(N_fft/2-1));

%   IIR filter calculation
for i = 1:length(f0)
    %   IIR filter coefficients
    a(i,:) = [1 (f0(i)^2+f0(i)*q-2) (1-f0(i)*q)]
    b(i,:) = [f0(i) -f0(i) 0]
    
    %   IIR filter output calculation
    y(i,:) = filter(b(i,:),a(i,:),u);
    
    %   IIR filter frequeny response
    [H_nf(i,:), f] = freqz(b(i,:), a(i,:), N_fft, 'whole', fs);
    Ha_nf(i,:) = abs(H_nf(i, 1:N_fft/2));
    Hp_nf(i,:) = 180*unwrap(angle(H_nf(i, 1:N_fft/2)))/pi;
end

%   Magnitude response plot
figure(1);
for i = 1:length(f0)
    plot (w, 20*log10(Ha_nf(i,:)), 'LineWidth', 1); axis ([0 fs/4 -40 20]); title('Magnitude response with parametrised frequency'); xlabel('Discrete frequency'); ylabel('Magnitude');
    hold on
end
%	Phase response plot
figure(2);
for i = 1:length(f0)
    plot (w, Hp_nf(i,:), 'LineWidth', 1); axis ([0 fs/4 -100 100]); title('Phase response with parametrised frequency'); xlabel('Discrete frequency'); ylabel('Angle');
    hold on
end
