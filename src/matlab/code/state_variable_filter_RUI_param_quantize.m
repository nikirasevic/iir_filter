clear all
clc

fs = 88200;     % Sample rate
n = 0 : 1999;   % Discrete time

fc = 5000;   % Filter central frequency in Hz
Q = 10;      % Filter Q factor (Q = fc/BW)

f0 = 2*sin((pi*fc)/fs)  % Central frequency coefficient for digital filter
q = 1/Q                 % Q factor coefficient for digital filter

%   Input signal definition
mag = [5 10 0 1 0];
f = [500 1000 2000 5000 8000];

x1 = mag(1)*cos(2 * pi * f(1)/fs * n);
x2 = mag(2)*cos(2 * pi * f(2)/fs * n);
x3 = mag(3)*cos(2 * pi * f(3)/fs * n);
x4 = mag(4)*cos(2 * pi * f(4)/fs * n);
x5 = mag(5)*cos(2 * pi * f(5)/fs * n);

u = x1 + x2 + x3 + x4 + x5;     % Input signal
u_sim = [n;u]';                 % Simulink workspace variable

%   Filter coeficients obtained from Input/Output equation
a = [1 (f0^2+f0*q-2) (1-f0*q)]
b = [f0 -f0 0]

%   IIR filter realization using function filter
y = filter(b,a,u);

%   Input and output signal plots
figure (1);
subplot(2,1,1), stem(n,u), title ('Ulazni signal u trajanju od 2000 odbiraka');
subplot(2,1,2), stem(n,y), title ('Izlazni signal u trajanju od 2000 odbiraka, racunat pomocu funkcije filter');

%	Frequency response calculation
N_fft = 1024;
n1 = 0:N_fft/2-1;
w = n1*fs/(2*(N_fft/2-1));

[H_nf, f] = freqz(b, a, N_fft, 'whole', fs);
Ha_nf= abs(H_nf(1:N_fft/2));
Hp_nf = 180*unwrap(angle(H_nf(1:N_fft/2)))/pi;

c = 1;
for num_bits = 8 : 23
    
    %	Quantization: fixed point, format 5+num_bits-2, quantization done
    %	with rounding
    struct.mode = 'fixed';
    struct.roundmode = 'round';
    struct.overflowmode = 'saturate';
    struct.format = [num_bits num_bits-5];
    q = quantizer(struct);
    
    %	Coefficient quantization
    a_q(c, :) = quantize (q, a);
    b_q(c, :) = quantize (q, b);
    
    %	Frequency response calculation of quantised filter
    [H_nf_q(c, :), f] = freqz(b_q(c, :), a_q(c, :), N_fft, 'whole', fs);
    Ha_nf_q(c, :) = abs(H_nf_q(c, 1:N_fft/2));
    Hp_nf_q(c, :) = 180*unwrap(angle(H_nf_q(c, 1:N_fft/2)))/pi;
    
    c = c + 1;
end

%   Magnitude response plot
colour = ['b', 'g', 'k', 'm'];
x_axis_limit = fs/8;

figure (2);
for i = 0 : 3
    subplot (2, 2, i+1), plot (w, 20*log10(Ha_nf), 'r', 'LineWidth', 1); axis ([0 x_axis_limit -40 40]);
    hold on;
    for j = 1 : 4
        subplot (2, 2, i+1), plot (w, 20*log10(Ha_nf_q(4*i+j,:)), colour(j), 'LineWidth', 1); axis ([0 x_axis_limit -40 40]);
        title('Quantization effect on the magnitude response'); xlabel('Discrete frequency'); ylabel('Magnitude');
    end
    legend ('Exact value', sprintf ('%d bits', 4*i+8), sprintf ('%d bits', 4*i+9), sprintf ('%d bits', 4*i+10), sprintf ('%d bits', 4*i+11),3);
end

%	Phase response plot
figure (3);
for i = 0 : 3
    subplot (2, 2, i+1), plot (w, Hp_nf, 'r', 'LineWidth', 1); axis ([0 x_axis_limit -100 100]);
    title('Quantization effect on the phase response'); xlabel('Discrete frequency'); ylabel('Angle');
    hold on;
    for j = 1 : 4
        subplot (2, 2, i+1), plot (w, Hp_nf_q(4*i+j,:), colour(j), 'LineWidth', 1);
    end
    legend ('Exact value', sprintf ('%d bits', 4*i+8), sprintf ('%d bits', 4*i+9), sprintf ('%d bits', 4*i+10), sprintf ('%d bits',4*i+11));
end