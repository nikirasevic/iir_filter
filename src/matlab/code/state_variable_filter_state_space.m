clear all
clc

fs = 88200;     % Sample rate
n = 0 : 1999;   % Discrete time

fc = 1000;   % Filter central frequency in Hz
Q = 10;      % Filter Q factor (Q = fc/BW)

f0 = 2*sin((pi*fc)/fs)  % Central frequency coefficient for digital filter
q = 1/Q                 % Q factor coefficient for digital filter

%	Single-input-single-output system, state space model
A = [(1-f0*q-f0^2) (-f0); f0 1]
B = [f0; 0]
C = [(1-f0*q-f0^2) (-f0)]
D = [f0]

%   Input signal definition
mag = [5 10 0 1 0];
f = [500 1000 2000 5000 8000];

x1 = mag(1)*cos(2 * pi * f(1)/fs * n);
x2 = mag(2)*cos(2 * pi * f(2)/fs * n);
x3 = mag(3)*cos(2 * pi * f(3)/fs * n);
x4 = mag(4)*cos(2 * pi * f(4)/fs * n);
x5 = mag(5)*cos(2 * pi * f(5)/fs * n);

u = x1 + x2 + x3 + x4 + x5;     % Input signal
u_sim = [n;u]';                 % Simulink workspace variable

%   System response calculation, initial conditions are zero
x(1:2, 1:length(n)+1) = 0;
y(1:length(n)+1) = 0;

for i = 2 : length(n)+1
x(:,i) = A*x(:,i-1)+B*u(i-1);
end

for i = 1 : length(n)
y(i) = C*x(:,i)+D*u(i);
end

y = y(1:length(n));

%   Input and output signal plots
figure(1);
subplot(2,1,1), stem(n,u), title ('Input signal of 2000 samples'); xlabel('Discrete time'); ylabel('Magnitude');
subplot(2,1,2), stem(n,y), title ('Output signal of 2000 samples, calculated with state space filter model, fc = 1000 Hz'); xlabel('Discrete time'); ylabel('Magnitude');
