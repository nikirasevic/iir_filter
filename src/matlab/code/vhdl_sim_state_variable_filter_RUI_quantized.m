clear all
clc

fs = 88200;     % Sample rate
n = 0 : 1999;   % Discrete time

fc = 1000;   % Filter central frequency in Hz
Q = 10;      % Filter Q factor (Q = fc/BW)

f0 = 2*sin((pi*fc)/fs)  % Central frequency coefficient for digital filter
q = 1/Q                 % Q factor coefficient for digital filter

%   Quantization number of bits
tot_len = 24;
rd_len = 20;

%   Input signal definition
mag = [0.05 0.1 0 0.01 0];
f = [500 1000 2000 5000 8000];

x1 = mag(1)*cos(2 * pi * f(1)/fs * n);
x2 = mag(2)*cos(2 * pi * f(2)/fs * n);
x3 = mag(3)*cos(2 * pi * f(3)/fs * n);
x4 = mag(4)*cos(2 * pi * f(4)/fs * n);
x5 = mag(5)*cos(2 * pi * f(5)/fs * n);

u = x1 + x2 + x3 + x4 + x5;     % Input signal
u_sim = [n;u]';                 % Simulink workspace variable

%   Filter coeficients obtained from Input/Output equation
a = [1 (f0^2+f0*q-2) (1 - f0*q)]
b = [f0 -f0 0]

%   IIR filter realization using function filter
y = filter(b,a,u);

%   Input and output signal plots
figure (1);
subplot(2,1,1), stem(n,u), title ('Input signal of 2000 samples'); xlabel('Discrete time'); ylabel('Magnitude');
subplot(2,1,2), stem(n,y), ylim([-15 15]); title ('Output signal of 2000 samples, calculated using function filter, fc = 1000 Hz'); xlabel('Discrete time'); ylabel('Magnitude');

a_dtf = [1 -(f0^2+f0*q-2) -(1 - f0*q)]
b_dtf = [f0 -f0 0]

%	Quantization: fixed point, format 5+num_bits-2, quantization done
%	with rounding
struct.mode = 'fixed';
struct.roundmode = 'round';
struct.overflowmode = 'saturate';
struct.format = [tot_len rd_len];
q = quantizer(struct);

%	Coefficient quantization
a_q = quantize (q, a)
b_q = quantize (q, b)

%   Quantized coefficients in hex
a_q_hex = num2hex(q,a_dtf)
b_q_hex = num2hex(q,b_dtf)

%digitalizacija diskretnog signala
u_digital = quantize(q,u);

%koeficijenti filtra
iir_ord = 2;

fileIDh = fopen('../../../filter_inputs/coef_hex_a.txt','w');
for i=1:iir_ord+1
    fprintf(fileIDh,'x"');
    fprintf(fileIDh,num2hex(q,a_dtf(i)));
    fprintf(fileIDh,'",\n');
end
fclose(fileIDh);

fileIDh = fopen('../../../filter_inputs/coef_hex_b.txt','w');
for i=1:iir_ord+1
    fprintf(fileIDh,'x"');
    fprintf(fileIDh,num2hex(q,b_dtf(i)));
    fprintf(fileIDh,'",\n');
end
fclose(fileIDh);

fileIDb = fopen('../../../filter_inputs/input.txt','w');
for i=1:length(u_digital)
    fprintf(fileIDb,num2bin(q,u(i)));
    fprintf(fileIDb,'\n');
end
fclose(fileIDb);

fileIDb = fopen('../../../filter_inputs/output.txt','w');
for i=1:length(u_digital)
    fprintf(fileIDb,num2bin(q,y(i)));
    fprintf(fileIDb,'\n');
end
fclose(fileIDb);

%   Quantised IIR filter realization using function filter
y_q = filter(b_q,a_q,u);

%   Input and output signal plots of quantised filter
figure (2);
subplot(2,1,1), stem(n,u_digital), title ('Input signal of 2000 samples'); xlabel('Discrete time'); ylabel('Magnitude');
subplot(2,1,2), stem(n,y_q), ylim([-15 15]);  title ('Quantised output signal of 2000 samples, calculated using function filter, fc = 1000 Hz'); xlabel('Discrete time'); ylabel('Magnitude');

%   IIR filter uantization error calculation and plot
delta = y-y_q;
delta_max = max(delta)

figure(3);
stem(n,delta), title ('Quantization error'); xlabel('Discrete time'); ylabel('Magnitude');
