clear all
clc

%fs = 88200;     % Sample rate
%n = 0 : 1999;   % Discrete time

%   Quantization number of bits
tot_len = 24;
rd_len = 20;

%   Input wav signal to be filtered
[u,fs] = audioread('../../../filter_inputs/wav/speech_dft.wav');

n = 0:length(u)-1;

fc = 1000;   % Filter central frequency in Hz
Q = 10;      % Filter Q factor (Q = fc/BW)

f0_fac = 2*sin((pi*fc)/fs)  % Central frequency coefficient for digital filter
q_fac = 1/Q                 % Q factor coefficient for digital filter

f0_fac_neg = - f0_fac;

%   Filter coeficients obtained from Input/Output equation
a = [1 (f0_fac^2+f0_fac*q_fac-2) (1 - f0_fac*q_fac)]
b = [f0_fac -f0_fac 0]

%   IIR filter realization using function filter
y = filter(b,a,u);

%   Input and output signal plots
%figure (1);
%subplot(2,1,1), stem(length(u(1))-1,u(1)), title ('Input signal of 2000 samples'); xlabel('Discrete time'); ylabel('Magnitude');
%subplot(2,1,2), stem(length(y)-1,y), ylim([-15 15]); title ('Output signal of 2000 samples, calculated using function filter, fc = 1000 Hz'); xlabel('Discrete time'); ylabel('Magnitude');

a_dtf = [1 -(f0_fac^2+f0_fac*q_fac-2) -(1 - f0_fac*q_fac)]
b_dtf = [f0_fac -f0_fac 0]

%	Quantization: fixed point, format 5+num_bits-2, quantization done
%	with rounding
struct.mode = 'fixed';
struct.roundmode = 'round';
struct.overflowmode = 'saturate';
struct.format = [tot_len rd_len];
q = quantizer(struct);

%   Quantized q and f0
q_fac_q = quantize (q, q_fac)
f0_fac_q = quantize (q, f0_fac)

%	Coefficient quantization
a_q = quantize (q, a)
b_q = quantize (q, b)

%   Quantized q and f0 in hex
q_fac_q_hex = num2hex(q,q_fac)
f0_fac_q_hex = num2hex(q,f0_fac)
f0_fac_neg_q_hex = num2hex(q,f0_fac_neg)

%   Quantized q and f0 in bin
q_fac_q_bin = num2bin(q,q_fac)
f0_fac_q_bin = num2bin(q,f0_fac)
f0_fac_neg_q_bin = num2bin(q,f0_fac_neg)

%   Quantized coefficients in hex
a_q_hex = num2hex(q,a_dtf)
b_q_hex = num2hex(q,b_dtf)

%   Quantized coefficients in binary
a_q_bin = num2bin(q,a_dtf)
b_q_bin = num2bin(q,b_dtf)

%digitalizacija diskretnog signala
u_digital = quantize(q,u);

u_digital_prim = u_digital';

%koeficijenti filtra
iir_ord = 2;

fileIDh = fopen('../../../filter_inputs/wav/coef_hex_a.txt','w');
for i=1:iir_ord+1
    fprintf(fileIDh,num2bin(q,a_dtf(i)));
    fprintf(fileIDh,'\n');
end
fclose(fileIDh);

fileIDh = fopen('../../../filter_inputs/wav/coef_hex_b.txt','w');
for i=1:iir_ord+1
    fprintf(fileIDh,num2bin(q,b_dtf(i)));
    fprintf(fileIDh,'\n');
end
fclose(fileIDh);

fileIDb = fopen('../../../filter_inputs/wav/input.txt','w');
for i=1:length(u_digital)
    fprintf(fileIDb,num2bin(q,u(i)));
    fprintf(fileIDb,'\n');
end
fclose(fileIDb);

fileIDb = fopen('../../../filter_inputs/wav/output.txt','w');
for i=1:length(u_digital)
    fprintf(fileIDb,num2bin(q,y(i)));
    fprintf(fileIDb,'\n');
end
fclose(fileIDb);

%   Quantised IIR filter realization using function filter
y_q = filter(b_q,a_q,u);

y_q_prim = y_q';

%   Input and output signal plots of quantised filter
%figure (2);
subplot(2,1,1), stem(n,u_digital_prim), title ('Input signal of 2000 samples'); xlabel('Discrete time'); ylabel('Magnitude');
subplot(2,1,2), stem(n,y_q_prim), ylim([-0.6 0.6]);  title ('Quantised output signal of 2000 samples, calculated using function filter, fc = 1000 Hz'); xlabel('Discrete time'); ylabel('Magnitude');

%   IIR filter uantization error calculation and plot
%delta = y-y_q;
%delta_max = max(delta)

%figure(3);
%stem(n,delta), title ('Quantization error'); xlabel('Discrete time'); ylabel('Magnitude');
